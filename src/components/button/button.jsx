import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './button.css';

const ButtonComponent = ({
    className,
    disabled,
    iconClassName,
    iconSrc,
    faIconClass,
    onClick,
    children,
    ...props
}) => {

    if (disabled) {
        onClick = function () {};
    }

    const icon = iconSrc && (
        <img
            className={classNames(iconClassName, styles.icon)}
            draggable={false}
            src={iconSrc}
        />
    );

    let faIcon = null;
    if (faIconClass) {
        faIcon = <i className={classNames("fa", faIconClass)}></i>;
    }

    return (
        <span
            className={classNames(
                styles.outlinedButton,
                className
            )}
            role="button"
            onClick={onClick}
            disabled={disabled ? true : false}
            {...props}
        >
            {icon}
            {faIcon}
            <div className={styles.content}>{children}</div>
        </span>
    );
};

ButtonComponent.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    faIconClass: PropTypes.string,
    disabled: PropTypes.bool,
    iconClassName: PropTypes.string,
    iconSrc: PropTypes.string,
    onClick: PropTypes.func
};

export default ButtonComponent;

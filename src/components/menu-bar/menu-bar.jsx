import classNames from 'classnames';
import bindAll from 'lodash.bindall';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import PropTypes from 'prop-types';
import React from 'react';

import Box from '../box/box.jsx';
import Button from '../button/button.jsx';
import {ComingSoonTooltip} from '../coming-soon/coming-soon.jsx';
import Divider from '../divider/divider.jsx';
import LanguageSelector from '../../containers/language-selector.jsx';
import ProjectLoader from '../../containers/project-loader.jsx';
import Menu from '../../containers/menu.jsx';
import {MenuItem, MenuSection} from '../menu/menu.jsx';
import ProjectSaver from '../../containers/project-saver.jsx';

import {openTipsLibrary} from '../../reducers/modals';
import {
    editorExpanded,
    openFileMenu,
    closeFileMenu,
    fileMenuOpen,
    openEditMenu,
    closeEditMenu,
    editMenuOpen,
    toggleEditorViewport,
    projectName,
    projectId,
    turboMode,
    changeProjectName,
    setTurboMode,
    saveProject,
    isSaving
} from '../../reducers/menus';

import styles from './menu-bar.css';

import mystuffIcon from './icon--mystuff.png';
import feedbackIcon from './icon--feedback.svg';
import profileIcon from './icon--profile.png';
import communityIcon from './icon--see-community.svg';
import dropdownCaret from '../language-selector/dropdown-caret.svg';
import scratchLogo from './scratch-logo.svg';

import helpIcon from './icon--help.svg';

const MenuBarItemTooltip = ({
    children,
    className,
    id,
    place = 'bottom'
}) => (
    <ComingSoonTooltip
        className={classNames(styles.comingSoon, className)}
        place={place}
        tooltipClassName={styles.comingSoonTooltip}
        tooltipId={id}
    >
        {children}
    </ComingSoonTooltip>
);

MenuBarItemTooltip.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    id: PropTypes.string,
    place: PropTypes.oneOf(['top', 'bottom', 'left', 'right'])
};

const MenuItemTooltip = ({id, children, className}) => (
    <ComingSoonTooltip
        className={classNames(styles.comingSoon, className)}
        place="right"
        tooltipClassName={styles.comingSoonTooltip}
        tooltipId={id}
    >
        {children}
    </ComingSoonTooltip>
);

MenuItemTooltip.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    id: PropTypes.string
};

const MenuBarMenu = ({
    children,
    onRequestClose,
    open,
    place = 'right'
}) => (
    <Menu
        className={styles.menu}
        open={open}
        place={place}
        onRequestClose={onRequestClose}
    >
        {children}
    </Menu>
);

MenuBarMenu.propTypes = {
    children: PropTypes.node,
    onRequestClose: PropTypes.func,
    open: PropTypes.bool,
    place: PropTypes.oneOf(['left', 'right'])
};

class MenuBar extends React.Component {
    constructor (props) {
        super(props);
        bindAll(this, [
            'handleSaveProjectToCloud',
            'handleNameTextFieldKeyUp',
            'handleLoadSavingHistories',
            'handleOnAboutClick'
        ]);
        this.autoSaveInterval = null;
    }

    handleSaveProjectToCloud (e) {
        const projId = this.props.projectId;
        const projName = this.props.projectName;
        const saveProjToCloud = this.props.onSaveProject;
        const renderer = this.props.vm.runtime.renderer;
        const projJsonStr = this.props.vm.toJSON();
        const manualSave = !!e;
        this.props.vm.saveProjectSb3().then(projContent => {
            renderer.draw();
            const thumbnail = renderer.gl.canvas.toDataURL();
            saveProjToCloud(projId, projJsonStr,
                            projName, projContent, thumbnail, manualSave);
        });
        //we could close the menu by code below:
        if (manualSave) {
            this.props.onRequestCloseFile();
        }
    }

    handleNameTextFieldKeyUp (e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            this.handleSaveProjectToCloud();
        }
    }

    handleLoadSavingHistories (e) {
        if (window.__scratch3_config &&
                window.__scratch3_config.onShowSavingHistories) {
            window.__scratch3_config.onShowSavingHistories(this.props.projectId);
        } else {
            alert("Not available");
        }
        this.props.onRequestCloseFile();
    }

    handleOnAboutClick (e) {
        if (window && window.__scratch3_config && window.__scratch3_config.onShowAboutInfo) {
            window.__scratch3_config.onShowAboutInfo();
        } else {
            alert("This project is a fork of Scratch 3.");
        }
        this.props.onRequestCloseFile();
    }

    componentDidMount () {
        const saveFunc = this.handleSaveProjectToCloud;
        let savingInterval = 30000; //30 seconds
        if (window && window.__scratch3_config && window.__scratch3_config.autoSavingInterval) {
            savingInterval = window.__scratch3_config.autoSavingInterval;
        }
        this.autoSaveInterval = setInterval(() => {
            // if not saved yet, no autosaving
            if (!this.props.projectId) {
                return;
            }
            // if project is running, sprite's attributes constantly change,
            //  such that the project.json changes, but those changes shouldn't
            //  be count as "changes" for auto-saving
            if (this.props.vm.projectIsRunning) {
                return;
            }
            saveFunc(null);
        }, savingInterval);
    }

    componentWillUnmount() {
        clearInterval(this.autoSaveInterval);
    }

    render() {
        const props = this.props;

        let btnToggleViewport = (
            <Button
                className={classNames(styles.toggleEditorViewportButton)}
                faIconClass={classNames(styles.toggleEditorViewportButtonIcon, "fa-expand")}
                onClick={props.onToggleEditorViewport}
                >
                <FormattedMessage
                    defaultMessage="Expand Editor"
                    description="Expand editor to full screen"
                    id="gui.menuBar.expandEditorViewport"
                    />
            </Button>
        );

        if (props.editorExpanded) {
            btnToggleViewport = (
                <Button
                    className={classNames(styles.toggleEditorViewportButton)}
                    faIconClass={classNames(styles.toggleEditorViewportButtonIcon, "fa-compress")}
                    onClick={props.onToggleEditorViewport}
                    >
                    <FormattedMessage
                        defaultMessage="Shrink Editor"
                        description="Shrink editor to normal size"
                        id="gui.menuBar.shrinkEditorViewport"
                        />
                </Button>
            )
        }

        let savingMenuItemMsg = null;
        let savingButtonMsg = null;
        if (props.isSaving) {
            savingMenuItemMsg = (
                <FormattedMessage
                    defaultMessage="Saving ..."
                    description="Saving project"
                    id="gui.menuBar.savingMenuItem"
                />);
            savingButtonMsg = (
                <FormattedMessage
                    defaultMessage="Saving ..."
                    description="Saving project"
                    id="gui.menuBar.savingButton"
                />);
        } else {
            savingMenuItemMsg = (
                <FormattedMessage
                    defaultMessage="Save Now"
                    description="Save project"
                    id="gui.menuBar.saveNowMenuItem"
                />);
            savingButtonMsg = (
                <FormattedMessage
                    defaultMessage="Save Now"
                    description="Save project"
                    id="gui.menuBar.saveNowButton"
                />);
        }

        let turboModeMsg = null;
        if (props.turboMode) {
            turboModeMsg = (<FormattedMessage
                defaultMessage="Turn off turbo mode"
                description="Menu bar item for turning off turbo mode"
                id="gui.menuBar.turnOffTurboMode"
            />);
        } else {
            turboModeMsg = (<FormattedMessage
                defaultMessage="Turn on turbo mode"
                description="Menu bar item for turning on turbo mode"
                id="gui.menuBar.turnOnTurboMode"
            />);
        }

        return (
            <Box className={styles.menuBar}>
                <div className={styles.mainMenu} id="menuBar">
                    <div className={styles.fileGroup}>
                        <div
                            className={classNames(styles.menuBarItem, styles.hoverable, {
                                [styles.active]: props.fileMenuOpen
                            })}
                            onMouseUp={props.onClickFile}
                        >
                            <div className={classNames(styles.fileMenu)}>
                                <FormattedMessage
                                    defaultMessage="File"
                                    description="Text for file dropdown menu"
                                    id="gui.menuBar.file"
                                />
                            </div>
                            <MenuBarMenu
                                open={props.fileMenuOpen}
                                onRequestClose={props.onRequestCloseFile}
                            >
                                <MenuSection>
                                    <MenuItem
                                        disabled={props.isSaving}
                                        onClick={this.handleSaveProjectToCloud}
                                        >
                                        {savingMenuItemMsg}
                                    </MenuItem>
                                    <MenuItem
                                        onClick={this.handleLoadSavingHistories}
                                        >
                                        <FormattedMessage
                                        defaultMessage="Load from saving history"
                                        description="Load history version"
                                        id="gui.menuBar.loadSavingHistory"
                                        />
                                    </MenuItem>
                                </MenuSection>

                                <MenuSection>
                                    <ProjectLoader>{(renderFileInput, loadProject, loadProps) => (
                                        <MenuItem
                                            onClick={(e) => {
                                                loadProject(e);
                                            }}
                                            {...loadProps}
                                        >
                                            <FormattedMessage
                                                defaultMessage="Upload from your computer"
                                                description="Menu bar item for uploading a project from your computer"
                                                id="gui.menuBar.uploadFromComputer"
                                            />
                                            {renderFileInput()}
                                        </MenuItem>
                                    )}</ProjectLoader>
                                    <ProjectSaver>{(saveProject, saveProps) => (
                                        <MenuItem
                                            onClick={(e) => {
                                                saveProject(e); props.onRequestCloseFile();
                                            }}
                                            {...saveProps}
                                        >
                                            <FormattedMessage
                                                defaultMessage="Download to your computer"
                                                description="Menu bar item for downloading a project"
                                                id="gui.menuBar.downloadToComputer"
                                            />
                                        </MenuItem>
                                    )}</ProjectSaver>
                                </MenuSection>
                                <MenuSection>
                                    <MenuItem onClick={this.handleOnAboutClick}>
                                        <FormattedMessage
                                            defaultMessage="About"
                                            description="About this new editor"
                                            id="gui.menuBar.about"
                                        />
                                    </MenuItem>
                                </MenuSection>
                            </MenuBarMenu>
                        </div>
                        <div
                            className={classNames(styles.menuBarItem, styles.hoverable, {
                                [styles.active]: props.editMenuOpen
                            })}
                            onMouseUp={props.onClickEdit}
                        >
                            <div className={classNames(styles.editMenu)}>
                                <FormattedMessage
                                    defaultMessage="Edit"
                                    description="Text for edit dropdown menu"
                                    id="gui.menuBar.edit"
                                />
                            </div>
                            <MenuBarMenu
                                open={props.editMenuOpen}
                                onRequestClose={props.onRequestCloseEdit}
                            >
                                <MenuItem
                                    disabled={(props.vm.mainWorkspace && props.vm.mainWorkspace.hasUndoStack()) ? false : true}
                                    onClick={() => {
                                        props.vm.mainWorkspace.undo();
                                        props.onRequestCloseEdit();
                                    }}>
                                    <FormattedMessage
                                        defaultMessage="Undo"
                                        description="Menu bar item for undoing"
                                        id="gui.menuBar.undo"
                                    />
                                </MenuItem>
                                <MenuItem
                                    disabled={(props.vm.mainWorkspace && props.vm.mainWorkspace.hasRedoStack()) ? false : true}
                                    onClick={() => {
                                        props.vm.mainWorkspace.undo(true);
                                        props.onRequestCloseEdit();
                                    }}>
                                    <FormattedMessage
                                        defaultMessage="Redo"
                                        description="Menu bar item for redoing"
                                        id="gui.menuBar.redo"
                                    />
                                </MenuItem>
                                <MenuSection>
                                    <MenuItem
                                        onClick={() => {
                                            const turboMode = props.turboMode;
                                            props.onSetTurboMode(!turboMode);
                                            props.vm.setTurboMode(!turboMode);
                                            props.onRequestCloseEdit();
                                        }}>
                                        {turboModeMsg}
                                    </MenuItem>
                                </MenuSection>
                            </MenuBarMenu>
                        </div>
                    </div>

                    {btnToggleViewport}

                    <Divider className={classNames(styles.divider)} />
                    <div className={classNames(styles.menuBarItem)}>
                        <input
                            className={classNames(styles.titleField)}
                            value={props.projectName}
                            onChange={(evt) => {props.onChangeProjectName(evt.target.value);}}
                            onKeyUp={this.handleNameTextFieldKeyUp}
                            id="title-field"
                        />
                    </div>

                    <div className={classNames(styles.menuBarItem)}>
                        <Button disabled={props.isSaving}
                            className={classNames(styles.saveButton)}
                            faIconClass={classNames(styles.saveButtonIcon, "fa-cloud-upload")}
                            onClick={this.handleSaveProjectToCloud}
                            >
                            {savingButtonMsg}
                        </Button>
                    </div>
                </div>
                <div className={styles.accountInfoWrapper}>
                    <div
                        aria-label="How-to Library"
                        className={classNames(styles.menuBarItem, styles.hoverable)}
                        onClick={props.onOpenTipLibrary}
                    >
                        <img
                            className={styles.helpIcon}
                            src={helpIcon}
                        />
                    </div>
                </div>
            </Box>
        );
    // end of render function
    }
};

MenuBar.propTypes = {
    editMenuOpen: PropTypes.bool,
    fileMenuOpen: PropTypes.bool,
    onToggleEditorViewport: PropTypes.func,
    onClickEdit: PropTypes.func,
    onClickFile: PropTypes.func,
    onOpenTipLibrary: PropTypes.func,
    onRequestCloseEdit: PropTypes.func,
    onRequestCloseFile: PropTypes.func,
    projectName: PropTypes.string,
    vm: PropTypes.shape({
        saveProjectSb3: PropTypes.func
    })
};

const mapStateToProps = state => ({
    fileMenuOpen: fileMenuOpen(state),
    editMenuOpen: editMenuOpen(state),
    editorExpanded: editorExpanded(state),
    projectName: projectName(state),
    projectId: projectId(state),
    vm: state.vm,
    isSaving: isSaving(state),
    turboMode: turboMode(state),
});

const mapDispatchToProps = dispatch => ({
    onOpenTipLibrary: () => dispatch(openTipsLibrary()),
    onClickFile: () => dispatch(openFileMenu()),
    onRequestCloseFile: () => dispatch(closeFileMenu()),
    onClickEdit: () => dispatch(openEditMenu()),
    onRequestCloseEdit: () => dispatch(closeEditMenu()),
    onToggleEditorViewport: () => dispatch(toggleEditorViewport()),
    onChangeProjectName: (name) => dispatch(changeProjectName(name)),
    onSaveProject: (projid, projJsonStr, name, content, thumbnail, manualSave) => {
        dispatch(saveProject(projid, projJsonStr, name, content, thumbnail, manualSave));
    },
    onSetTurboMode: (on) => dispatch(setTurboMode(on))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MenuBar);

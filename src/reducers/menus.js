import axios from 'axios';
import md5 from 'js-md5';

const OPEN_MENU = 'scratch-gui/menus/OPEN_MENU';
const CLOSE_MENU = 'scratch-gui/menus/CLOSE_MENU';
const TOGGLE_EDITOR = 'scratch-gui/menus/TOGGLE_EDITOR';
const CHANGE_PROJ_NAME = 'scratch-gui/menus/CHANGE_PROJ_NAME';
const CHANGE_SAVED_PROJ_NAME = 'scratch-gui/menus/CHANGE_SAVED_PROJ_NAME';
const SAVE_PROJ_FAILED = 'scratch-gui/menus/SAVE_PROJ_FAILED';
const SET_PROJ_ID = 'scratch-gui/menus/SET_PROJ_ID';
const SET_REMIX_PROJ_ID = 'scratch-gui/menus/SET_REMIX_PROJ_ID';
const SET_TURBO_MODE = 'scratch-gui/menus/SET_TURBO_MODE';
const SET_PROJ_JSON_HASH = 'scratch-gui/menus/SET_PROJ_JSON_HASH';

const MENU_FILE = 'fileMenu';
const MENU_EDIT = 'editMenu';
const EDITOR_EXPANDED = 'editorExpanded'
const PROJECT_NAME = 'projectName'
const SAVED_PROJECT_NAME = 'savedProjectName'
const SAVING_PROJ = 'savingProject'
const PROJ_SAVED = 'projectSaved'
const IS_SAVING = 'isSaving'
const PROJECT_ID = 'projectId'
const REMIX_PROJECT_ID = 'remixProjectId'
const PROJECT_JSON_HASH = 'projectJsonHash'
const TURBO_MODE = 'turboMode'

const DEFAULT_PROJECT_NAME = "Untitled Project"


const initialState = {
    [MENU_FILE]: false,
    [MENU_EDIT]: false,
    [EDITOR_EXPANDED]: false,
    [PROJECT_NAME]: DEFAULT_PROJECT_NAME,
    [SAVED_PROJECT_NAME]: null,
    [IS_SAVING]: false,
    [TURBO_MODE]: false,
    [PROJECT_ID]: null,
    [REMIX_PROJECT_ID]: null,
    [PROJECT_JSON_HASH]: null
};

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch (action.type) {
    case OPEN_MENU:
        return Object.assign({}, state, {
            [action.menu]: true
        });
    case CLOSE_MENU:
        return Object.assign({}, state, {
            [action.menu]: false
        });
    case TOGGLE_EDITOR:
        if (window && window.__scratch3_config
                   && window.__scratch3_config.onToggleEditorSize) {
           window.__scratch3_config.onToggleEditorSize(state[[action.menu]]);
        }
        return {
            ...state,
            [action.menu]: !state[[action.menu]]
        }
    case CHANGE_PROJ_NAME:
        return {
            ...state,
            [PROJECT_NAME]: action.name
        }
    case CHANGE_SAVED_PROJ_NAME:
        return {
            ...state,
            [SAVED_PROJECT_NAME]: action.name
        }
    case SAVING_PROJ:
        return {
            ...state,
            [IS_SAVING]: true
        };
    case PROJ_SAVED:
        if (typeof(window.notify) !== "undefined") {
            notify.suc("Your project has been saved");
        } else {
            console.log("Your project has been saved");
        }
        if (action.data.isCreating) {
            const editURL = "/project/" + action.data.projectId + "/edit";
            if (typeof(window.__scratch3_config) !== "undefined") {
                window.history.pushState(null, "Edit project", editURL);
                if (typeof(window.appcommons) !== "undefined") {
                    const appRoot = window._app_root || "";
                    appcommons.sync_url(null, appRoot + editURL);
                }
            } else {
                console.log("Change url to:", editURL);
            }
        }
        return {
            ...state,
            [IS_SAVING]: false,
            [PROJECT_ID]: action.data.projectId,
            [REMIX_PROJECT_ID]: null,
            [SAVED_PROJECT_NAME]: action.data.projectName,
            [PROJECT_JSON_HASH]: action.data.projectJsonHash
        }
    case SAVE_PROJ_FAILED:
        if (typeof(window.notify) !== "undefined") {
            notify.err(action.error);
        } else {
            console.log(action.error);
        }
        return {
            ...state,
            [IS_SAVING]: false
        }
    case SET_PROJ_ID:
        return {
            ...state,
            [PROJECT_ID]: action.projectId
        }
    case SET_REMIX_PROJ_ID:
        return {
            ...state,
            [REMIX_PROJECT_ID]: action.remixProjectId
        }
    case SET_TURBO_MODE:
        return {
            ...state,
            [TURBO_MODE]: action.on
        }
    case SET_PROJ_JSON_HASH:
        return {
            ...state,
            [PROJECT_JSON_HASH]: action.projectJsonHash
        }
    default:
        return state;
    }
};
const openMenu = menu => ({
    type: OPEN_MENU,
    menu: menu
});
const closeMenu = menu => ({
    type: CLOSE_MENU,
    menu: menu
});
const toggleEditor = menu => ({
    type: TOGGLE_EDITOR,
    menu: menu
});


// state
const fileMenuOpen = state => state.menus[MENU_FILE];
const editMenuOpen = state => state.menus[MENU_EDIT];
const editorExpanded = state => state.menus[EDITOR_EXPANDED];
const projectName = state => state.menus[PROJECT_NAME];
const isSaving = state => state.menus[IS_SAVING];
const projectId = state => state.menus[PROJECT_ID];
const turboMode = state => state.menus[TURBO_MODE];

// action msg
const openFileMenu = () => openMenu(MENU_FILE);
const closeFileMenu = () => closeMenu(MENU_FILE);
const openEditMenu = () => openMenu(MENU_EDIT);
const closeEditMenu = () => closeMenu(MENU_EDIT);
const toggleEditorViewport = () => toggleEditor(EDITOR_EXPANDED);
const changeProjectName = (newName) => ({type:CHANGE_PROJ_NAME, name: newName});
const changeSavedProjectName = (newName) => ({type:CHANGE_SAVED_PROJ_NAME, name: newName});
const setProjectID = (toID) => ({type:SET_PROJ_ID, projectId: toID});
const setRemixProjectID = (toID) => ({type:SET_REMIX_PROJ_ID, remixProjectId: toID});
const setProjectJsonHash = (toHash) => ({type:SET_PROJ_JSON_HASH, projectJsonHash: toHash});
const setTurboMode = (on) => ({type:SET_TURBO_MODE, on: on});
const saveProject = (projId, projJsonStr, projName, projContent, projThumbnail, manualSave) => {
    return (dispatch, getState) => {
        // if already have projectId, then we are in "edit" mode
        // otherwise, we are in "create" mode
        const isCreating = projId ? false : true;
        const currState = getState();
        const projectJsonHash = md5(projJsonStr);
        const remixProjId = currState.menus[REMIX_PROJECT_ID];
        const savedProjName = currState.menus[SAVED_PROJECT_NAME];
        if (currState.menus[PROJECT_JSON_HASH] === projectJsonHash
                    && projName === savedProjName) {
            if (manualSave) {
                // if user click "Save" manually, show "saved" message directly
                dispatch({
                    type: PROJ_SAVED,
                    data: {
                        isCreating: false,
                        projectId: projId,
                        projectName: projName,
                        projectJsonHash: projectJsonHash
                    }
                });
            }
            return;
        }

        dispatch({type:SAVING_PROJ});

        if (window && window.__scratch3_config && window.__scratch3_config.apiSaveProject) {
            const params = new FormData();
            const instance = axios.create({
                headers: {'content-type': 'multipart/form-data'}
            });
            if (projId) {
                params.append("projectid", projId);
            }
            if (remixProjId) {
                params.append("remix_projectid", remixProjId);
            }
            params.append("name", projName);
            params.append("content", projContent);
            params.append("thumbnail", projThumbnail);

            instance.post(
                window.__scratch3_config.apiSaveProject,
                params
            ).then(function (resp) {
                const ret = resp.data;
                if (ret.ok) {
                    dispatch({
                        type: PROJ_SAVED,
                        data: {
                            isCreating: isCreating,
                            projectId: ret.info.projectid,
                            projectName: projName,
                            projectJsonHash: projectJsonHash
                        }
                    });
                } else {
                    dispatch({
                        type: SAVE_PROJ_FAILED,
                        error: ret.info
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            // fake saving
            setTimeout(function () {
                dispatch({
                    type: PROJ_SAVED,
                    data: {
                        isCreating: isCreating,
                        projectId: "fake-id-123456",
                        projectName: projName,
                        projectJsonHash: projectJsonHash
                    }
                });
            }, 1000);
        }
    }
}


export {
    reducer as default,
    openFileMenu,
    closeFileMenu,
    openEditMenu,
    closeEditMenu,
    fileMenuOpen,
    editMenuOpen,
    editorExpanded,
    toggleEditorViewport,
    projectName,
    projectId,
    turboMode,
    changeProjectName,
    changeSavedProjectName,
    setProjectID,
    setRemixProjectID,
    setTurboMode,
    setProjectJsonHash,
    saveProject,
    isSaving
};

import GoogleAnalytics from 'react-ga';

/*
GoogleAnalytics.initialize(process.env.GA_ID, {
    debug: (process.env.NODE_ENV !== 'production'),
    titleCase: true,
    sampleRate: (process.env.NODE_ENV === 'production') ? 100 : 0,
    forceSSL: true
});

export default GoogleAnalytics;
*/

const emptyFunc = function () {};

const ShadowGoogleAnalytics = {
    initialize: emptyFunc,
    set: emptyFunc,
    pageview: emptyFunc,
    modalview: emptyFunc,
    event: emptyFunc,
    ga: emptyFunc,
    outboundLink: emptyFunc,
    exception: emptyFunc,
    plugin: {
        require: emptyFunc,
        execute: emptyFunc
    }
};

export default ShadowGoogleAnalytics;

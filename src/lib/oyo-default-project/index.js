import {TextEncoder} from 'text-encoding';
import projectJson from './project.json';

import backdrop from '!arraybuffer-loader!./739b5e2a2435f6e1ec2993791b423146.png';
import costume1 from '!raw-loader!./e4fc8716cb98b1e2e2e1d9f6a9a75b1c.svg';


const encoder = new TextEncoder();
export default [{
    id: 0,
    assetType: 'Project',
    dataFormat: 'JSON',
    data: JSON.stringify(projectJson)
}, {
    id: '739b5e2a2435f6e1ec2993791b423146',
    assetType: 'ImageBitmap',
    dataFormat: 'PNG',
    data: new Uint8Array(backdrop)
}, {
    id: 'e4fc8716cb98b1e2e2e1d9f6a9a75b1c',
    assetType: 'ImageVector',
    dataFormat: 'SVG',
    data: encoder.encode(costume1)
}];

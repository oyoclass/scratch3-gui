import React from 'react';
import ReactDOM from 'react-dom';

import Box from '../components/box/box.jsx';
import GUI from '../containers/gui.jsx';

import axios from 'axios';
import async from 'async-es';
import JSZip from 'jszip';

if (process.env.NODE_ENV === 'production' && typeof window === 'object') {
    // Warn before navigating away
    window.onbeforeunload = () => true;
}

import styles from './player.css';
const Player = () => (
    <Box className={styles.stageOnly}>
        <GUI
            isPlayerOnly
            isFullScreen={false}
        />
    </Box>
);

const appTarget = document.createElement('div');

let appContainer = document.body;
if (window && window.__scratch3_config && window.__scratch3_config.container) {
    appContainer = window.__scratch3_config.container;
}
appContainer.appendChild(appTarget);

if (window && window.__scratch3_config &&
        typeof(window.__scratch3_config.projectId) !== "undefined" &&
        window.__scratch3_config.projectId) {
    const projId = window.__scratch3_config.projectId;
    const projFetchURL = window.__scratch3_config.apiProjectJson;
    const projParams = {"projectId": projId, "playerOnly": 1};
    axios.get(
        projFetchURL,
        {
            params: projParams
        }
    )
    .then(function (response) {
        const {
            projectId,
            projectName,
            projectJson,
            projectFileNames,
            projectBaseURL,
            projectVersion,
            projectFileNameDict
        } = response.data.info;
        const axiosInst = axios.create({
                baseURL: projectBaseURL,
                responseType: "arraybuffer"
            });
        const fetchRequests = [];

        projectFileNames.forEach((fname) => {
            fetchRequests.push(function (callback) {
                axiosInst.get(fname)
                    .then(function (response) {
                        const data = {
                            "filename": fname,
                            "filecontent": response.data
                        }
                        callback(null, data)
                    });
                    /*
                    .catch(function (err) {
                        callback(err);
                    })
                    */
            });
        });

        async.parallel(fetchRequests, function (err, results) {
            if (err) {
                console.log(err);
                return;
            }
            // results contains all resources' arraybuffer
            var zip = new JSZip();
            zip.file("project.json", projectJson);
            results.forEach(function (item) {
                let fname = item.filename;
                if (projectVersion === 2) {
                    fname = projectFileNameDict[fname];
                }
                zip.file(fname, item.filecontent);
            });
            zip.generateAsync({type: "blob"})
                .then(function(content) {
                    const fileReader = new FileReader();
                    fileReader.onload = function (event) {
                        ReactDOM.render(
                            <Box>
                                <GUI
                                    projectData={fileReader.result}
                                    projectID={projectId}
                                    projectName={projectName}
                                    isPlayerOnly
                                    isFullScreen={false}
                                />
                            </Box>, appTarget);
                    };
                    fileReader.readAsArrayBuffer(content);
                });
        });

    }).catch(function (error) {
         console.log(error);
    });
} else {
    ReactDOM.render(<Player />, appTarget);
}

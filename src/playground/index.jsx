import 'es6-object-assign/auto';
import React from 'react';
import ReactDOM from 'react-dom';

import analytics from '../lib/analytics';
import GUI from '../containers/gui.jsx';

import styles from './index.css';

import axios from 'axios';
import async from 'async-es';
import JSZip from 'jszip';

if (process.env.NODE_ENV === 'production' && typeof window === 'object') {
    // Warn before navigating away
    window.onbeforeunload = () => true;
}

// Register "base" page view
analytics.pageview('/');

const appTarget = document.createElement('div');
appTarget.className = styles.app;

let appContainer = document.body;
if (window && window.__scratch3_config && window.__scratch3_config.container) {
    appContainer = window.__scratch3_config.container;
}
appContainer.appendChild(appTarget);

GUI.setAppElement(appTarget);


if (window && window.__scratch3_config &&
        typeof(window.__scratch3_config.projectId) !== "undefined" &&
        window.__scratch3_config.projectId) {
    const projId = window.__scratch3_config.projectId;
    const projFetchURL = window.__scratch3_config.apiProjectJson;
    const projParams = {
        "projectId": projId
    };
    if (window.__scratch3_config.historyVersion) {
        projParams["historyVersion"] = window.__scratch3_config.historyVersion;
    }
    if (window.__scratch3_config.isRemixMode) {
        projParams["remix"] = 1;
    }
    axios.get(
        projFetchURL,
        {
            params: projParams
        }
    )
    .then(function (response) {
        const {
            projectId,
            projectName,
            projectJson,
            projectFileNames,
            projectBaseURL,
            projectVersion,
            projectFileNameDict
        } = response.data.info;
        const axiosInst = axios.create({
                baseURL: projectBaseURL,
                responseType: "arraybuffer"
            });
        const fetchRequests = [];

        projectFileNames.forEach((fname) => {
            fetchRequests.push(function (callback) {
                axiosInst.get(fname)
                    .then(function (response) {
                        const data = {
                            "filename": fname,
                            "filecontent": response.data
                        }
                        callback(null, data)
                    });
                    /*
                    .catch(function (err) {
                        callback(err);
                    })
                    */
            });
        });

        async.parallel(fetchRequests, function (err, results) {
            if (err) {
                console.log(err);
                return;
            }
            // results contains all resources' arraybuffer
            var zip = new JSZip();
            zip.file("project.json", projectJson);
            results.forEach(function (item) {
                let fname = item.filename;
                if (projectVersion === 2) {
                    fname = projectFileNameDict[fname];
                }
                zip.file(fname, item.filecontent);
            });
            zip.generateAsync({type: "blob"})
                .then(function(content) {
                    const fileReader = new FileReader();
                    fileReader.onload = function (event) {
                        // remix mode, don't set project ID
                        // user will create new project based on remix code
                        if (projParams["remix"]) {
                            ReactDOM.render(<GUI
                                projectData={fileReader.result}
                                remixProjectID={projectId}
                                projectName={projectName}
                                />, appTarget);

                        } else {
                            ReactDOM.render(<GUI
                                projectData={fileReader.result}
                                projectID={projectId}
                                projectName={projectName}
                                />, appTarget);
                        }
                    };
                    fileReader.readAsArrayBuffer(content);
                });
        });

    }).catch(function (error) {
         console.log(error);
    });
} else {
    ReactDOM.render(<GUI />, appTarget);
}
